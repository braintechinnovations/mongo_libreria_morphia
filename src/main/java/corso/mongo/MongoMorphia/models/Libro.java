package corso.mongo.MongoMorphia.models;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Field;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Reference;
import org.mongodb.morphia.annotations.Validation;

@Entity("Libri")
@Indexes({ @Index(fields = @Field("title")) })
@Validation("{ costo : { $gt : 0 }}")
public class Libro {

	@Id
	private String isbn;
	
	@Property("title")
	private String titolo;
	
	@Property
	private String autore;
	
	@Property("costo")
	private double prezzo;
	
	@Property
	private LocalDateTime pubDate;
	
	@Embedded
	private Editore editore;
	
	@Reference
	private Set<Libro> correlati = new HashSet<Libro>();

	public Libro() {
//		this.correlati = new HashSet<Libro>();
	}
	
	public Libro(String isbn, String titolo, String autore, double prezzo, Editore editore) {
		super();
		this.isbn = isbn;
		this.titolo = titolo;
		this.autore = autore;
		this.prezzo = prezzo;
		this.editore = editore;
//		this.correlati = new HashSet<Libro>();
	}
	
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getAutore() {
		return autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	public LocalDateTime getPubDate() {
		return pubDate;
	}

	public void setPubDate(LocalDateTime pubDate) {
		this.pubDate = pubDate;
	}

	public Editore getEditore() {
		return editore;
	}

	public void setEditore(Editore editore) {
		this.editore = editore;
	}

	public Set<Libro> getCorrelati() {
		return correlati;
	}

	public void setCorrelati(Set<Libro> correlati) {
		this.correlati = correlati;
	}

	@Override
	public String toString() {
		return "Libro [isbn=" + isbn + ", titolo=" + titolo + ", autore=" + autore + ", prezzo=" + prezzo + ", pubDate="
				+ pubDate + ", editore=" + editore + ", correlati=" + correlati + "]";
	}
	
	
}
