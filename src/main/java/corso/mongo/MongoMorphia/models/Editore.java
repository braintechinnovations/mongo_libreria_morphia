package corso.mongo.MongoMorphia.models;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

@Entity("Editori")
public class Editore {

	@Id
	private ObjectId id;
	
	@Property
	private String nome;
	
	@Property
	private String citta;

	public Editore() {
		
	}
	
	public Editore(ObjectId id, String nome, String citta) {
		super();
		this.id = id;
		this.nome = nome;
		this.citta = citta;
	}



	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	@Override
	public String toString() {
		return "Editore [id=" + id + ", nome=" + nome + ", citta=" + citta + "]";
	}
	
	
	
}
