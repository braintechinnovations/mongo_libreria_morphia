package corso.mongo.MongoMorphia;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.converters.LocalDateTimeConverter;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

import corso.mongo.MongoMorphia.models.Editore;
import corso.mongo.MongoMorphia.models.Libro;

class Credenziali{
	static String username = "";
	static String password = "";
	static String database = "Libreria";
	static String address = "localhost";
	static int port = 27017;
}

public class App 
{
    public static void main( String[] args )
    {

//    	MongoCredential mongoCredential = MongoCredential.createCredential(
//    			Credenziali.username, 
//				Credenziali.database, 
//				Credenziali.password.toCharArray()
//				);
//    	
//    	MongoClient client = new MongoClient(
//    			new ServerAddress(Credenziali.address, Credenziali.port),
//    			
//    			Arrays.asList(mongoCredential)
//    			);
    	
    	//Opzionale con connection String
    	MongoClientURI uri = new MongoClientURI("mongodb://utentemongo:qpzyC9Mbm6SEwEA4@cluster0-shard-00-00.7ttvy.mongodb.net:27017,cluster0-shard-00-01.7ttvy.mongodb.net:27017,cluster0-shard-00-02.7ttvy.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-6wmih5-shard-0&authSource=admin&retryWrites=true&w=majority");
    	MongoClient client = new MongoClient(uri);
    	
    	Morphia morphia = new Morphia();
    	
    	try {
	    	morphia.mapPackage("corso.mongo.MongoMorphia.models");
	    	Datastore ds = morphia.createDatastore(client, Credenziali.database);
	    	ds.ensureIndexes();
	    	
//	    	Editore editore = new Editore(new ObjectId(), "Edizioni programming", "Milano");
//	    	
//	    	Libro libro = new Libro("1234-4567-1234", "La Tempesta", "W.S", 10.5d, editore);
//	    	
//	    	Libro puoInteressare = new Libro("1234-4567-1235", "Othello", "W.S", 11.5d, editore);
//	    	
//	    	libro.setCorrelati(new HashSet<Libro>());
//	    	libro.getCorrelati().add(puoInteressare);
//	    	
//	    	ds.save(editore);
//	    	ds.save(puoInteressare);
//	    	ds.save(libro);
	    	
//	    	System.out.println("Operazione completata");
	    	
	    	//Ricerca di uno o più oggetti
	    	List<Libro> elenco = ds.find(Libro.class)
	    			.field("titolo")
	    			.contains("Tempesta")
	    			.asList();
	    	
	    	for(int i=0; i<elenco.size(); i++) {
	    		Libro temp = elenco.get(i);
	    		System.out.println(temp);
	    	}
	    	
	    	
    	} catch (Exception e) {
    		System.out.println(e.getMessage());
		}
    	

    }
}
